#!/usr/bin/python
'''
This script reads the data from the database and creates graphs one at a time well by well. 
It takes either an imput file or command line input. Both should be formatted as the following:
wells/elements/start end [/ file_name]
Where, 
file_name to save the command line arguments to read them again. Gefault is default_cmd.csv
start start date for the graph in unix format
end end date for the graph if no provided, now() is used as default
elements list of elements separated by white space
wells list of wells separated by white space
Examples:
./build_graph.py R-1 'R-33 S1' / Cr Manganese / 2015-01-01 /
./build_graph.py R-1 'R-33 S1' / Mn / 2015-01-01 2016-01-01/
'''
import sys
import mysql.connector
import datetime
import os.path
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

'''
__author__ = "Lucia Short"
__copyright__ = "Copyright 2015/16, Los Alamos National Security, LLC"
__credits__ = ["Lucia Short"]

__version__ = "0.0.1"
__status__ = "Production"
'''

def parseInput(data):
    # We are listing the wells
    # date from to
    # the chemicals
    in_set = {
            'locations': [],
            'elements': [],
            'date': ()
            }
    in_set['locations'] = data[0]
    in_set['elements'] = data[1]
    if len(data[2]) == 1:
        in_set['date'] = (data[2][0],)
    if len(data[2]) == 2:
        in_set['date'] = (data[2][0],data[2][1]) 
    return in_set
    
def mysqlConnect(query):
    ''' Connect the database and query the table
    '''

    config = {
        'user': 'perl',
        'password': 'script',
        'host': '127.0.0.1',
        'database': 'LocalWork',
        'raise_on_warnings': True
        }
    dataset = {
            'result': [],
            'date': [],
            'units':[]
            }

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor(buffered=True)
    #query = ("Explain WellsAnalyses;")

    cursor.execute(query)
    for result, units, time in cursor:
        dataset['result'].append(result)
        dataset['units'].append(units)
        dataset['date'].append(time)

    cursor.close()
    cnx.close()
    return dataset

def getQuery(dataset):
    '''Form query using the dictionary that is passed as an argument
    '''
    legend = [x + " " + y for x in dataset['locations'] for y in dataset['elements']]
    names = [x.replace(" ", "") for x in legend]
    names = [x.replace("-", "_") for x in names]
    print names
    query = "SELECT "
    for name in names:
        query = query + name + ".result, " + name + ".units, " + name + ".collection_date_time"
        if name is not names[-1]:
            query = query + ","
        query = query + " "
        
    query = query + "FROM "
    
    for name in names:
        query = query + "WellsAnalyses " + name
        if name is not names[-1]:
            query = query + ","
        query = query + " "
        
    query = query + "WHERE "

    # Get locations
    for lc in dataset['locations']:
        tb_name = lc.replace(" ", "")
        tb_name = tb_name.replace("-", "_")
        tb_names = [a for a in names if tb_name in a]
        for nm in tb_names:
            query = query + "(" + nm + ".Location='" + lc + "' OR " + nm + ".Location_Alias='" + lc +"') "
            if nm not in tb_names[-1]:
                query = query + "AND "
        if lc not in dataset['locations'][-1]:
            # TODO This one might have be "AND"
            query = query + "OR "
    
    # Get elements
    query = query + "AND CAS='Cr' "

    # Get time
    query = query + "AND collection_date_time BETWEEN '" + dataset['date'][0] + "' AND "
    if dataset['date'][1] is not None:
        query = query + "'" + dataset['date'][1] + "' "
    else:
        query = query + "now() "
    query = query + "ORDER BY collection_date_time ASC;"
    return query

def formQuery(location, element, start, end):
    '''Form query to the database and call getData()
    '''
    query_array = ["SELECT result, units, collection_date_time FROM WellsAnalyses WHERE (Location='",
            location,
            "' OR Location_Alias='" + location + "') AND (CAS='",
            element + "' OR Analyte='" + element + "') ",
            " AND collection_date_time BETWEEN '" + start + "' AND "]
    if end is not None and end is not '':
        query_array.append("'" + end + "'")
    else:
        query_array.append("now()")
    query_array.append(" ORDER BY collection_date_time ASC;")
                                                                                                                                    
    query = "".join(query_array)
    print "\nFull query"
    print query
    return query

def plotData(xy, title, lgnd, ylabels):
    '''Plot received data
    '''
    fig, ax = plt.subplots()
    #ax.plot(x,y,'o-r', label=lgnd)
    #check if ylabels are different
    labels = [k for k in ylabels if not k == ylabels[0]]
    if len(labels) > 0:
        lgnd = [a + ", " + b for a in lgnd for b in ylabels]
    for i in range(len(xy)):
        ax.plot(xy[i][0],xy[i][1],label=lgnd[i])

    # form ylabel
    if len(labels) < 1:
        ax.set_ylabel(ylabels[0])
    # rotate the data
    fig.autofmt_xdate()
    # form title
    plt.title(title)
    # turn on the legend
    plt.legend()
    # turn on the grid
    plt.grid(True)

    # save plt
    plt.savefig("fig1.pdf", format='pdf')
    plt.show()

def parseArguments(args):
    ''' Parse arguments
    if there is only one argument, then it is the name of the file
    otherwise the input should be separated with the separator
    '''
    
    tmp = [[],[],[], []]
    command_line_history = "default_cmd.csv"
    print "Parsing arguments...\n"

    if len(args) == 1:
        # check if the file exists
        if os.path.isfile(args[0]):
            ff = open(args[0], 'r')
            input_arr = ff.readline().rstrip(' \n').split(' ')
            ff.close()
            args = input_arr + args
        else:
            print "No such file as " + args[0]
            sys.exit()

    # go through the arguments and create the input data
    i = 0
    separator = '/'
    for entry in args:
        if entry == separator:
            i = i+1
        else:
            tmp[i].append(entry)
    if not tmp[-1]:
        tmp[-1].append(command_line_history)

    ff=open(tmp[-1][0], 'w')
    for iterator in range(0,len(tmp)-1):
        for word in tmp[iterator]:
            ff.write(word + " ")
        ff.write(separator + " ")
    ff.write("\n")
    ff.close
    #print 'Parsed arguments:'
    #print tmp
    return tmp

    # else the argumens have to contain:
    # -l locations, -e elements, -d date(from, to) to can be not there

def check_units(unit_set):
    unit = unit_set[0]
    other_units = [ k for k in unit_set if not k == unit]
    if len(other_units) > 0:
        print "ERROR:"
        print "\tInconsistent UNITS. Besides of units " + unit + " other units are:"
        print other_units
    return unit


if __name__ == '__main__':
    #--------------------------------------------------------------------------------------
    # parse command line arguments
    # if there is no arguments, 
    # read the file
    # if there are no arguments or file
    # print error

    # No arguments found
    if len(sys.argv) == 1:
        print "*******************************************************************************"
        print "\t\tNo arguments"
        print "\t\t-------------"
        print "Please enter the name of the file with the command line:"
        print "\t./build_graph.py input.txt"
        print "or enter arguments wells / element / start end [ / file_name ]"
        print " in the command line, separated by '/' and ' ':"
        print "\t./build_graph.py R-1 'R-33 S1' / Cr Manganese / 2015-01-01 /"
        print "*******************************************************************************"
        sys.exit()

    # Arguments check
    input_args = parseArguments(sys.argv[1:])
    input_set = parseInput(input_args)
    print "\nInput set:"
    print input_set

    #--------------------------------------------------------------------------------------
    # form the query for the database on madsmax
    # we would need:
    # 1. well's location or alias: WellsAnalyses.Location or WellsAnalyses.Location_Alias
    location="I-5"
    # 2. element CAS/Analyte
    element="Cr"
    # 3. WellsAnalyses.Result and WellsAnalyses.Units
    # 4. Collection_Date_Time
    start = '2011-01-01'
    end = None
    #--------------------------------------------------------------------------------------
    # query the database using input_set set
    location = input_set['locations'][0]
    element = input_set['elements'][0]
    start = input_set['date'][0]
    end = input_set['date'][1]
    for location in input_set['locations']:
        ylabels=list()
        xy_data = list()
        for element in input_set['elements']:
            query = formQuery(location, element, start, end)
            dataset = mysqlConnect(query)
            ylabels.append(check_units(dataset['units']))
            xy_data.append([dataset['date'], dataset['result']])

        #dataset = parseData(dataset)
        #--------------------------------------------------------------------------------------
        # parse the data
        # y=dataset['result']
        # x=dataset['date']
        #--------------------------------------------------------------------------------------
        # plot the data
        title = "Location: " + location
        legend = input_set['elements']
        plotData(xy_data, title, legend, ylabels)