mysql -u perl -pscript < query.mysql > query.dat
# egrep "\tPM-|\tO-" query.dat | sort -k6 | awk '{printf "%s %.8g %.8g %g %g\n", $6, $1*.3048, $2*.3048, ($3-$5)*.3048, ($3-$4)*.3048}' | grep -i i | grep "$1"
egrep "PM-|O-" query.dat | sort -k6 | awk '{printf "%s %.8g %.8g %g %g\n", $6, $1*.3048, $2*.3048, ($3-$5)*.3048, ($3-$4)*.3048}' | grep "$1"
