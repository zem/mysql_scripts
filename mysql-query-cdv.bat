mysql -u perl -pscript < query.mysql > query.dat
# egrep " CDV-" query.dat | sort -k6 | grep -v "Temp" | sed "s/#//;s/Piezometer//" | gawk '{split($6,w,"-");split($8,s," ");printf "%s%s %s_%s %.8g %.8g %g %g\n", w[2],s[1],$6,s[1],$1*.3048, $2*.3048, ($3-$4)*.3048,($3-$5)*.3048}' | grep -v i | sort -k 1n | cut -f 2- -d " " | grep "$1"
egrep -i "R-25 |R-18 |R-58|R-48 |R-47 |R-47i |R-63 |R-29 |R-19 |CDV" query.dat | sort -k 6 \
 | awk '{printf "%s%s %.2f %.2f %.2f %.2f\n", $6, $8, $1*.3048, $2*.3048, ($3-$4)*.3048, ($3-$5)*.3048 }'
