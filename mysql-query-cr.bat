mysql -u perl -pscript < query.mysql > query.dat
# egrep "R-1 |R-6 |R-7 |R-8 |R-10a |R-11 |R-13 |R-15 |R-16r |R-28 |R-33 |R-34 |R-35|R-36|R-37 " query.dat | sort -k 6
# egrep "R-1 |R-6 |R-7 |R-8 |R-10a |R-11 |R-13 |R-15 |R-16r |R-28 |R-33 |R-34 |R-35|R-36|R-37 " query.dat | sort -k 6 | awk '{print $6, $7, $8, $9, $1*.3048, $2*.3048 ($3-$4)*.3048, ($3-$5)*.3048 }'
# egrep "R-1 |R-11 |R-13 |R-15 |R-28 |R-33 |R-34 |R-35|R-36 |R-42 |R-43 |R-44 |R-45 |R-50 |R-61 |R-62 |Cr" query.dat | grep "#1" | sort -k 6  | sed "s/Screen #1//"\
egrep "R-1 |R-11 |R-13 |R-15 |R-28 |R-33 |R-34 |R-35|R-36 |R-42 |R-43 |R-44 |R-45 |R-50 |R-61 |R-62 |Cr" query.dat | sort -k 6 \
| awk '{printf "%s%s %.2f %.2f %.2f %.2f\n", $6, $8, $1*.3048, $2*.3048, ($3-$4)*.3048, ($3-$5)*.3048 }'
