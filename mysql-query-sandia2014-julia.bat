# mysql -u perl -pscript < query.mysql > query.dat
mysql -u root  < query.mysql > query.dat
# egrep "R-1 |R-11|R-13|R-15|R-28|R-33|R-34|R-35|R-36|R-42|R-43|R-44|R-45|R-50|R-61|R-62" query.dat | sort -k6
egrep "R-1 |R-11|R-13|R-15|R-28|R-33|R-34|R-35a|R-36|R-42|R-43|R-44|R-45|R-50|R-61|R-62" query.dat | sort -k6 | awk '{print$6,$7,$8,$1*.3048,$2*.3048,($3-$4)*.3048,($3-$5)*.3048}' |grep "#1"
egrep "R-1 |R-11|R-13|R-15|R-28|R-33|R-34|R-35a|R-36|R-42|R-43|R-44|R-45|R-50|R-61|R-62" query.dat | sort -k6 | awk '{print "%s %s %.8g %.8g\n",$6,$8,$1*.3048,$2*.3048}' | grep "#1"
egrep "R-1 |R-11|R-13|R-15|R-28|R-33|R-34|R-35a|R-36|R-42|R-43|R-44|R-45|R-50|R-61|R-62" query.dat | sort -k6 | awk '{print "%s %s %.8g %.8g\n",$6,$8,$1*.3048,$2*.3048}' | grep "#1" | awk '{ printf "\"%s\"=>(%.8g,%.8g),\n", $1, $3, $4}'
# egrep "MCOI|SCI" query.dat | sort -k6 | awk '{print$6,$7,$8,$9,$1*.3048,$2*.3048}' | grep "#1"
