#! /usr/bin/env perl 
use DBI;

$numArgs = $#ARGV + 1;
if($numArgs < 1) {
	die "USAGE: well-water-levels [list of wells[_'screen_number':'baro_corr'~'max_drop]] [[dates]]\n";
}
$date = 0;
$begin = 0;
$end = 0;
for($i=0;$i<$numArgs;$i++){
	$ARGV[$i]=~ tr/[a-z]/[A-Z]/;
	if($ARGV[$i] !~ /[A-Z]/ ) {
		if( $date == 0 ) { $begin = $ARGV[$i] }
		else { $end = $ARGV[$i]; }
		$date++;
	}
	else {
		# Collect max drop between successive pressure readings
		if( $ARGV[$i] =~ /~/ ) {
			$maxdrop[$i]=$ARGV[$i];
			$maxdrop[$i]=~s/(.+)~(.+)/$2/; 
			$ARGV[$i]=~s/(.+)~(.+)/$1/; }
		else {  $maxdrop[$i]=1000; }
		if( $ARGV[$i] =~ /:/ ) {
			$bcorr[$i]=$ARGV[$i];
			$bcorr[$i]=~s/(.+):(.+)/$2/; 
			$ARGV[$i]=~s/(.+):(.+)/$1/; }
		else {  $bcorr[$i]=1; }
		if( $ARGV[$i] !~ /_/ ) {
			$wells[$i]=$ARGV[$i];
			$portdesc[$i]=0;
		}
		else {
			$wells[$i]=$ARGV[$i];
			$wells[$i]=~ s/(.+)\_([0-9]*)/$1/;
			$portdesc[$i]=$ARGV[$i];
			$portdesc[$i]=~ s/(.+)\_([0-9]*)/$2/;
		}
		print "Well $wells[$i] Port $portdesc[$i] Baro $bcorr[$i] MaxDrop $maxdrop[$i]\n";
	}
	print "$ARGV[$i]\n";
}
print "\n";
if( $begin == 0 ) { $begin = 19450101; }
else { print "Begin date $begin\n"; }
if( $end == 0 ) { $end = 20190101; }
else { print "End   date $end\n"; }

# Create mysql connection variables
my $driver   = "mysql";
my $server   = "localhost";
my $database = "LocalWork";
my $url      = "DBI:$driver:$database:$server";
my $user     = "perl";
my $password = "script";

# Connect to database using handle
my $dbh = DBI->connect( $url, $user, $password ); 

# Initialize variables
my($time);
my($ftime) = 0;	# Earliest pumping time
my($lprodtime) = 1e20;		# Latest pumping time
my($rate);	# Flow rate variable
my(@srate);
my(@sttime);
my($count) = 0;
my($prev_zero) = 0;

for($i=0;$i<@wells;$i++){
	print "Monitoring well $wells[$i] Port $portdesc[$i] ";
	#
	# Collect well elevation
	$sth2 = $dbh->prepare("SELECT SURFACE_ELEVATION,ELEV_UOM FROM WQDBLocation WHERE LOCATION_NAME LIKE \'$wells[$i]\'");
	$sth2->execute() or die "SQL Error: $DBI::errstr\n";
	@elevwell = $sth2->fetchrow_array();
	if ($elevwell[1] ne ft) { 
		die "Surface elevation units $elevwell[1] not feet for $wells[$i]\n";
	} 
	if ($sth2->rows == 0) {
		print "No well elevaiton match for $wells[$i]\n";
	}
	$sth2 = ();
	#
	# Prepare and execute query for ALL head measurements
	#
	if ($portdesc[$i] eq '0') {
		$sth3 = $dbh->prepare("SELECT datediff(time,\'1899-12-30\'),time,piezometricWLft,temperatureC,time(time) FROM GroundWaterLevelData WHERE probeno < 10 AND wellname REGEXP \'^$wells[$i]\$\' AND dataqualcode REGEXP \'^\$|V|VR|VQ|VRVQ\' and time > $begin AND time < $end GROUP BY time");
	} else {
		$sth3 = $dbh->prepare("SELECT datediff(time,\'1899-12-30\'),time,piezometricWLft,temperatureC,time(time) FROM GroundWaterLevelData WHERE portdesc REGEXP \'$portdesc[$i]\' AND probeno < 10 AND wellname REGEXP \'^$wells[$i]\$\' AND dataqualcode REGEXP \'^\$|V|VR|VQ|VRVQ\' and time > $begin AND time < $end GROUP BY time");
	}
	$sth3->execute() or die "SQL Error: $DBI::errstr\n";
	if ($sth3->rows == 0) {
	        die print "No observations for observation well $wells[$i] at port $portdesc[$i]!\n";
	}
	# Open PLT file for plotting pumping well production
	if ($portdesc[$i] eq '0') { $pltflnm = "$wells[$i]-water-levels.dat"; }
	else { $pltflnm = "$wells[$i]_$portdesc[$i]-water-levels.dat"; }
	open(pltfl, ">$pltflnm") || die "Can't open PLT file: $pltflnm\n"; 

	$count = 0;
	$mintime = 1e20;
	$maxtime = 0;
	$obstimelast = 0;
	$headlast = 0;
	while(@entry = $sth3->fetchrow_array())
	{
		# Convert time to decimal days and add to days since 1900
		my(@time) = split(/:/, $entry[4]); # time
		my($dectime) = (($time[2]/60 + $time[1])/60 + $time[0])/24;
		$obstime = $entry[0] + $dectime;
		
		if($count>0) { $diffday = $obstime-$obstimelast; }
		else { $diffday = 0; }
		# Convert feet to meters
		$uncheads = $entry[2]*0.3048; # Save uncorrected heads
		# Correct for barometric pressure
		if($bcorr[$i]>0) {
			$heads = &AdjBaroTA54($obstime,$entry[1],$entry[2],$entry[3],$elevwell[0],$bcorr[$i])*0.3048;
			if($heads == 0)	{
				$obstimelast = $obstime;
				next;
			}
			# Shift corrected heads according to calibrated reading, likely the first reading after a break in readings
			if($count==0||$diffday>1.1) {
				$adjcorr = $uncheads - $heads;
				$heads = $uncheads;
			} else {
				$heads = $heads+$adjcorr;
			}
		}
		else { $heads = $uncheads; }
		if($diffday>1.1) {print pltfl "\n";}
		# Check for sampling of well - large sudden pressure drop
		if($count > 0 && (($headlast - $heads) > $maxdrop[$i])) {
			print pltfl "\n";
			# Do not update headlast, the sampling event may take longer than 1 time step!
		} else { 
			$o = $obstime + 2415019;
			print pltfl "$obstime $o $uncheads $heads\n";
			if( $obstime > $maxtime ) { $maxtime = $obstime; }
			if( $obstime < $mintime ) { $mintime = $obstime; }
			$headlast = $heads;
			$obstimelast = $obstime;
		}
		$count++;
	}
	print "Begin $mintime End $maxtime Number of records $count\n";
}
close(pltfl);

for($i=0;$i<@wells;$i++) {
	if ($portdesc[$i] eq '0') { $name = $wells[$i]; }
	else { $name = "$wells[$i]_$portdesc[$i]"; }
	my($pltflnm) = "$name-water-levels.dat";
	@args = ("head-plot-reg","$pltflnm","$name","lines");
	system(@args) == 0 || die "system @args failed!!!\n";
	
}

sub AdjBaroTA54AVGset {
	# Collect barometric pressure at TA-54 at $gwtime
        $sth2 = $dbh->prepare("SELECT datediff(time,\'1899-12-30\'),AVG(pressmb),tempC FROM TA54BaroData GROUP BY date(time)"); 
	$sth2->execute() or die "SQL Error: $DBI::errstr\n";
	if ($sth2->rows == 0) { print "No barometric pressure data !\n"; return 0; }
	@data = $sth2->fetchrow_array();
	return 1;
}


sub AdjBaroTA54AVGrun {
	# Create variable for TA54 elevation
	my($elevta54) = 6548;
	
	# Create variable for average barometric pressure for TA54 (calculated from database)
	my($avgpress) = 26.79;	
		
	# Assign arguments to variables
	my($tdec) = $_[0];
	my($gwtime) = $_[1];
	my($uncorrWL) = $_[2];
	my($welltemp) = $_[3];
	my($elevwell) = $_[4];
	my($baroeff) = $_[5];

	# Collect values
#	print "new $tdec, $data[0], \n";
	if( $tdec < $data[0] ) { print "Missing barometric data for TA-54 near $gwtime $tdec!!!\n"; return 0; } 
	while( $tdec > $data[0] ) {
		if( $tdec == $data[0] ) { last; }
		@data = $sth2->fetchrow_array();
	}
#	print "col $data[0], $data[1], $data[2], $data[3], \n";

        $pressmb = $data[1];

	# Convert mbar to feet
	my($pressft) = $pressmb*0.0145037*2.304;
	
	# Calculate the barometric pressure in the well at $time
	my($presswell) = $pressft*exp(-9.80665/(3.281*287.04)*(($elevwell-$elevta54)/(275.1)+($uncorrWL-$elevwell)/($welltemp+273.15)));

	# Calculate the average barometric pressure in the well (average temp for all wells is used for avg well temp)
	my($avgpresswell) = $avgpress*exp(-9.80665/(3.281*287.04)*(($elevwell-$elevta54)/(275.1)+(6590-$elevwell)/(287.4)));	

	# Calculate corrected water level
	my($presscorr) = $avgpresswell-$presswell;
	my($corrWL) = $uncorrWL-$presscorr*$baroeff;

	return $corrWL;
}

sub AdjBaroTA54 {
	# Create variable for TA54 elevation
	my($elevta54) = 6548;
	
	# Create variable for average barometric pressure for TA54 (calculated from database)
	my($avgpress) = 26.79;	
		
	# Assign arguments to variables
	my($tdec) = $_[0];
	my($gwtime) = $_[1];
	my($uncorrWL) = $_[2];
	my($welltemp) = $_[3];
	my($elevwell) = $_[4];
	my($baroeff) = $_[5];

	# Collect barometric pressure at TA-54 at $gwtime
        my($sth54) = $dbh->prepare("SELECT datediff(time,\'1899-12-30\'),time(time),pressmb,tempC FROM TA54BaroData WHERE time BETWEEN ADDTIME(\'$gwtime\',\'-0 00:15:00\') AND ADDTIME(\'$gwtime\',\'0 00:15:00\')"); 
	$sth54->execute() or die "SQL Error: $DBI::errstr\n";
	if ($sth54->rows == 0) { print "No barometric pressure data at $gwtime!\n"; $sth54 = (); return 0; };
			
	# Collect upper and lower values for interpation
	my(@lower) = $sth54->fetchrow_array();
	my(@upper) = $sth54->fetchrow_array();

	# Convert time to decimal days and add to days since 1900
	my(@time) = split(/:/, $lower[1]);
	my($dectime) = (($time[2]/60 + $time[1])/60 + $time[0])/24;
	my($ltime) = $lower[0] + $dectime;

	# Convert time to decimal days and add to days since 1900
	my(@time) = split(/:/, $upper[1]);
	my($dectime) = (($time[2]/60 + $time[1])/60 + $time[0])/24;
	my($utime) = $upper[0] + $dectime;

	# Check that @lower and @upper exist
        if($lower[2] == 0 && $upper[2] != 0) {
                $pressmb = $upper[2];
        } elsif($upper[2] == 0 && $lower[2] != 0) {
                $pressmb = $lower[2];
        } elsif($lower[2] == 0 && $upper[2] == 0) {
                print "Missing barometric data for TA-54 near $gwtime $utime!!!\n";
		$sth54 = ();
		return 0;
        } else {
                # Perform linear interpolation
                $pressmb = ($tdec-$ltime)*($upper[2]-$lower[2])/($utime-$ltime)+$lower[2];
#                $tempta54 = ($tdec-$ltime)*($upper[3]-$lower[3])/($utime-$ltime)+$lower[3];
        }

	# Convert mbar to feet
	my($pressft) = $pressmb*0.0145037*2.304;
	
	# Calculate the barometric pressure in the well
	my($presswell) = $pressft*exp(-9.80665/(3.281*287.04)*(($elevwell-$elevta54)/(275.1)+($uncorrWL-$elevwell)/($welltemp+273.15)));

	# Calculate the average barometric pressure in the well (average temp for all wells is used for avg well temp)
	my($avgpresswell) = $avgpress*exp(-9.80665/(3.281*287.04)*(($elevwell-$elevta54)/(275.1)+(6590-$elevwell)/(287.4)));	

	# Calculate corrected water level
	my($presscorr) = $avgpresswell-$presswell;
	if($presscorr > 0.2 ) { $sth54 = (); return $uncorrWL; }
	my($corrWL) = $uncorrWL-$presscorr*$baroeff;
	
	$sth54 = ();

	return $corrWL;
}
